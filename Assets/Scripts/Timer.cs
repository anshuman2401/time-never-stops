﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    int time = 20;
    private Text timeText;

    public GameObject playerObj;

    // Start is called before the first frame update
    void Start()
    {
        timeText = GetComponent<Text>();
        StartCoroutine(timeReducer());  
    }

    private void FixedUpdate()
    {
        int minutes = time / 60;
        int seconds = time % 60;
        string timeShow = "";
        ;
        if (minutes <= 9)
        {
            timeShow = "0";

        }

        timeShow += minutes.ToString();
        timeShow += ":";

        if (seconds <= 9)
        {
            timeShow += "0";
        }

        timeShow += seconds.ToString();

        timeText.text = timeShow;

    }

    IEnumerator timeReducer()
    {
        while(true)
        {
            yield return new WaitForSecondsRealtime(1f);

            time -= 1;

            if(time <= 0)
            {
                // Destroy Player if time is 0
                playerObj.SetActive(false);
                timeText.enabled = false;
            }
        }
    }

    public void decreaseTime(int amount)
    {
        time -= amount;
    }

    public void increaseTime(int amount)
    {
        time += amount;
    }
}
