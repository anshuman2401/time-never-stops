﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyPatrol : MonoBehaviour
{
    private Rigidbody2D m_Body;
    private float dist = 2f;
    private bool movingRight = false;

    public float speed = 5f;
    public Transform groundCheck;

    // Start is called before the first frame update
    void Start()
    {
        m_Body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        m_Body.velocity = new Vector3(-transform.right.x * speed * Time.deltaTime
                          * 10f, 0f, 0f);

        RaycastHit2D groundInfo = Physics2D.Raycast(groundCheck.position, Vector2.down , dist);

        if (groundInfo.collider == false)
        {
            if(movingRight)
            {
                transform.eulerAngles = new Vector3(0, 180f, 0);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 00f, 0);
                movingRight = true;
            }
        }

    }
}
