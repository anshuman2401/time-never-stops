﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : MonoBehaviour
{
    private Rigidbody2D m_Body;

    public float hitForce = 10f;
    public float health = 100f;
    public float damage = 50f;
    public GameObject timeIncreaser;

    private void Awake()
    {
        m_Body = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Power"))
        {
            m_Body.AddForce(collision.gameObject.transform.right * hitForce);

            health -= damage;

            if(health <= 0f)
            {
                // Spawn a time increaser
                Instantiate(timeIncreaser, transform.position, Quaternion.identity);

                Destroy(gameObject);
            }
        }
    }
}
