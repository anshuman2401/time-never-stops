﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class IceBall : MonoBehaviour
{
    private Rigidbody2D m_Body;
   
    public float fireSpeed = 10f;


    private void Awake()
    {
        m_Body = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        m_Body.velocity = new Vector3(transform.right.x * fireSpeed * Time.deltaTime * 100f, 0f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.tag.Equals("Player"))
        {
            Destroy(gameObject);
        }
    }
}
