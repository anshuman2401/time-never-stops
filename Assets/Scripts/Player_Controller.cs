﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Player_Motor))]
public class Player_Controller : MonoBehaviour
{

    private bool jump;
    private float horizontal_dir = 0f;
    private Timer timerScript;

    public Player_Motor player_Motor;
    public float speed = 25f;
    public GameObject power;
    public GameObject timer;
    public int decreaseTimeAfterFire = 3;

    private void Awake()
    {
        timerScript = timer.GetComponent<Timer>();
    }

    // Update is called once per frame
    void Update()
    {   
        horizontal_dir = Input.GetAxisRaw("Horizontal") * speed;

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            jump = true;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            fire();
        }

    }

    private void FixedUpdate()
    {
        player_Motor.move(horizontal_dir * Time.fixedDeltaTime, jump);
        jump = false;
    }

    void fire()
    { 
        Instantiate(power, transform.position, transform.rotation);

        // Decrease timer by 5 seconds
        timerScript.decreaseTime(decreaseTimeAfterFire);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            // Destroy Player
            gameObject.SetActive(false);
        }
    }

}
