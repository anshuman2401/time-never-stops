﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeIncreaserGem : MonoBehaviour
{
    private Timer timerScript;

    private void Awake()
    {
        timerScript = GameObject.FindWithTag("Timer").GetComponent<Timer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            timerScript.increaseTime(5);

            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision);
    }
}
